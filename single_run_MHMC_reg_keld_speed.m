%N = 10;
%run_number = 0;
%fname1 = 'testo_testo_y_n.out';
%fname2 = 'testo_testo_Fs.out';
%fname3 = 'testo_testo_avg_x_n.out';

%N, run_number, fname1, fname2, fname3 should be defined outside
t_final = 2.5;
timesteps = N + 1;
alpha = 0 + 1i;
lambda = 10;

%gets x_t for initialization
exact_no_regulator;
clearvars -except x_t run_number N fname1 fname2 fname3 t_final timesteps alpha lambda timespace

%initialize
dt = t_final/N;
epsilon = 5*dt;

if N == 10
    numOfCalcs = 5*10^5;
    %just put something here so I can test
    stdo = .15;
elseif N == 15
    numOfCalcs = 5*10^6;
    stdo = .1;
elseif N == 20
    numOfCalcs = 1*10^7;
    stdo = .075;
elseif N == 25
    numOfCalcs = 5*10^7;
    stdo = .04;
else
    numOfCalcs = 1*10^8;
    stdo = .02;
end

%initialize calculation variables
when_to_export = 5*10^1;
y_n = transpose(real(x_t)+.01);
capture_y_n = zeros(when_to_export, length(y_n));
capture_Fs = zeros(when_to_export,1);
changes = 0;

% calculation step 0
[dFs, Fs] = distrib_function(y_n, alpha, dt, epsilon, lambda);
x_n = y_n * Fs;
norm = Fs;
capture_y_n(1,:) = transpose(y_n);
capture_Fs(1) = Fs;

% calculations
for j = 1:numOfCalcs
    if j == round((numOfCalcs)/5)
        disp('a fifth is done');
    elseif j == round((numOfCalcs)/5) * 2
        disp('two fifths are done');
    elseif j == round((numOfCalcs)/5) * 3
        disp('three fifths are done');
    elseif j == round((numOfCalcs)/5) * 4
        disp('four fifths are done');
    end
    
    %After 'when_to_export' updates, lets export the data to some files
    if mod(j,when_to_export) == 0
        dlmwrite(fname1, capture_y_n, '-append', 'delimiter', ' ', 'roffset', 0);
        dlmwrite(fname2, capture_Fs, '-append', 'delimiter', ' ', 'roffset', 0);
        
        capture_y_n = zeros(when_to_export, length(y_n));
        capture_Fs = zeros(when_to_export,1);
    end
    
    %update
    [dFs,Fs,y_n,changes] = update_step(dFs, Fs, y_n, changes, alpha, dt, epsilon, lambda, j, stdo);
    x_n = y_n * Fs + x_n;
    norm = Fs + norm;
    
    capture_y_n(mod(j,when_to_export)+1,:) = transpose(y_n);
    capture_Fs(mod(j,when_to_export)+1) = Fs;
end
%export the last of the data
dlmwrite(fname1, capture_y_n(1:mod(j,when_to_export)+1,:), '-append', 'delimiter', ' ', 'roffset', 0);
dlmwrite(fname2, capture_Fs(1:mod(j,when_to_export)+1), '-append', 'delimiter', ' ', 'roffset', 0);

x_n = x_n/norm;
%output the total found x_n
fout = fopen(fname3, 'w');
fprintf(fout,'%f\n',x_n);
fclose(fout);

disp(x_n);
disp(['changes: ', num2str(changes/numOfCalcs * 100), '%']);

%plot
%{
figure('units','normalized','outerposition',[0 0 1 1]);
plot(linspace(0,t_final,N+1), transpose(x_n));
xlabel('time, t');
ylabel('Expectation of x')
title(['Lambda = ', num2str(lambda),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(epsilon/dt), ', Run: ', num2str(run_number)]);
hold on;
plot(timespace, real(x_t), '-.');
legend('monte carlo integration', 'exact (no epsilon)');
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dFsNew, FsNew, y_f, changes] = update_step(dFs, Fs, y_f, changes, alpha, delta, epsilon, lambda, index, stdo)
    %scales range to fit nicely between min and max std limits
    scalefactor = .4;
    %sets min and max std limits
    facto = 10;
    min_std = stdo / facto;
    max_std = stdo * facto;
    %holds which fourier basis to update
    current = mod(index,length(y_f));
    %holds the basis
    %proposal_vec = cos(pi*(0:(length(y_f)-1))/(length(y_f)-1) * current)/sqrt(length(y_f));
    proposal_vec = get_proposal_vec(length(y_f), current);
    
    %get range of current position/deriv
    %[dFscurr,Fscurr] = distrib_function(y_f, alpha, delta, epsilon_samp, lambda);
    direc_deriv = sum(dFs .* transpose(proposal_vec));
    range = abs(Fs/direc_deriv) * scalefactor;
    %bound it
    if range > max_std
        range = max_std;
    end
    if range < min_std
        range = min_std;
    end
    
    %use the range to propose new step
    random_jump = real(normrnd(0, range));
    proposal = transpose(proposal_vec) * random_jump;
    %calculate update's range
    [dFsPropose,FsPropose] = distrib_function(y_f + proposal, alpha, delta, epsilon, lambda);
    %[dFsprop,Fsprop] = distrib_function(y_f + proposal, alpha, delta, epsilon_samp, lambda);
    direc_deriv_prop = sum(dFsPropose .* transpose(proposal_vec));
    propose_range = abs(FsPropose/direc_deriv_prop) * scalefactor;
    %bound it, also if we had an update's deriv AND func to be 0, we are at a fringe, so just never accept.
    if isnan(propose_range)
        propose_range = 0;
        y_f = y_f;
        dFsNew = dFs;
        FsNew = Fs;
        return;
    end
    if propose_range > max_std
        propose_range = max_std;
    end
    if propose_range < min_std
        propose_range = min_std;
    end
    
    %calc prob of acceptance based on current position
    prob = abs(FsPropose/Fs);
    %additional conditional prob based on different derivs/update stds
    cond_prob = (normpdf(random_jump, 0, propose_range)/1) / (normpdf(random_jump, 0, range)/1);
    %together, find the prob
    prob = min(1, prob * cond_prob);
    
    %accept or not
    random = rand;
    if random <= prob
        y_f = y_f + proposal;
        dFsNew = dFsPropose;
        FsNew = FsPropose;
        changes = changes + 1;
    else
        dFsNew = dFs;
        FsNew = Fs;
    end
end

function [dFs, Fs] = distrib_function(y_f, alpha, delta, epsilon, lambda)
    limit = 10^50;
    deriv_limit = limit;
    
    min_limit = 10^-10;
        
    %Fixing issues with y_n = 0
    for i = 1:length(y_f)
        if abs(y_f(i)) == 0
            y_f(i) = 10^-4;
        end
        if abs(y_f(i)) < 10^-4
            y_f(i) = 10^-4 * sign(y_f(i));
        end
    end
    %}

    %%%%% Distribution Function's Setup %%%%%
    ddoty = derivative_2nd(y_f,delta);
    %A_n
    A_n = (-delta) * (2*ddoty + 2*y_f + 8*lambda*y_f.^3);
    A_n(1) = (-delta) * (2*ddoty(1) + 2*y_f(1) + 8* lambda *y_f(1).^3) + 2*sqrt(2)*imag(alpha) - 2*(y_f(2)-y_f(1))/delta;
    %B_n
    B_n = ones(size(A_n)) * -epsilon;
    B_n(1) = -1 - epsilon;
    %C_n
    C_n = (-delta)*8*lambda*y_f;
    %G_n
    G_n = A_n + B_n.^2 ./ (3*C_n);
    
    %%%%% Distribution Funcion %%%%%
    %
    Fs = ones(length(y_f)-1, 1);
    
    R = G_n./real_1_by_3(3*C_n);
    if abs(y_f(end)) < .01
        %disp('goofy');
    end
    for j = 1:(length(y_f)-1)
        Fs(j) = 2 * pi * real(airy(0,R(j),1));  
        Fs(j) = Fs(j) * real(exp((-2/3)*(R(j)).^(3/2) - (A_n(j).*B_n(j))./(3*C_n(j)) - (2*B_n(j).^3)./(27*C_n(j).^2)) ./ abs(3*C_n(j)).^(1/3));
    end
    
    Fs = exp(-(y_f(1) - sqrt(2)*real(alpha))^2) * prod(Fs(1:end)); %* exp (-(sqrt(2)*imag(alpha) - (y_f(2) - y_f(1))/delta)^2);
    
    %function fixing
    if isnan(Fs)
        Fs = 0;
    end
    if Fs == inf
        Fs = limit*sign(Fs);
    end
    if Fs == -inf
        Fs = limit*sign(Fs);
    end
    if abs(Fs) > limit
        Fs = limit*sign(Fs);
    end
    
    %%%%% Derivative setup %%%%%
    d_C_n = -delta * 8 * lambda * ones(size(y_f));
    d_A_n = -delta * (2*(-2/delta^2) + 2 + 24*lambda*y_f.^2);
    %because of forward difference
    d_A_n(1) = -delta * (2*(1/delta^2) + 2 + 24*lambda*y_f(1)^2) + 2/delta;
    d_G_n = d_A_n - (B_n.^2 ./ (3*C_n.^2)).*d_C_n;
    
    L_n = ((A_n.*B_n)./(3*C_n.^2) + 4*B_n.^3 ./(27*C_n.^3) - 1./(3*C_n)) .* d_C_n;
    M_n_0 = -B_n./(3*C_n) .* d_A_n;
    K_n_0 = 1./real_1_by_3(3*C_n) .* d_G_n + G_n./(-3*C_n.*real_1_by_3(3*C_n)) .* d_C_n;
    
    d_A_n_m1 = -2*delta*(1/delta^2) * ones(size(A_n));
    %because of forward difference, & A(1) has a normal derivative term
    d_A_n_m1(1) = -2*delta*(-2/delta^2) - 2/delta;
    d_G_n_m1 = d_A_n_m1;
    %recall that M_0_m1 and K_0_m1 aren't used
    M_n_m1 = [0;B_n]./(3 * [0;C_n]) .* [0;d_A_n_m1];
    M_n_m1 = -M_n_m1(1:end-1);
    K_n_m1 = 1 ./ real_1_by_3([0;3*C_n]) .* [0;d_G_n_m1];
    K_n_m1 = K_n_m1(1:end-1);
       
    d_A_n_m1 = -2*delta*(1/delta^2) * ones(size(A_n));
    M_2_m2 = [0;0;B_n]./(3 * [0;0;C_n]) .* [0;0;d_A_n_m1];
    M_2_m2 = -M_2_m2(3);
    K_2_m2 = 1 ./ real_1_by_3([0;0;3*C_n]) .* [0;0;d_A_n_m1];
    K_2_m2 = K_2_m2(3);

    d_A_n_p1 = -2*delta*(1/delta^2) * ones(size(A_n));
    d_G_n_p1 = d_A_n_p1;
    %M_N-1_p1 and K_N-1_p1 aren't used
    M_n_p1 = [B_n;0]./(3 * [C_n;0]) .* [d_A_n_p1;0];
    M_n_p1 = -M_n_p1(2:end);
    K_n_p1 = 1 ./ real_1_by_3([3*C_n;0]) .* [d_G_n_p1;0];
    K_n_p1 = K_n_p1(2:end);
    
    %%%%% Derivative Function %%%%%
    dFs = zeros(size(y_f));
    for j = 1:length(y_f)
        %y_0
        if j == 1
            dFs(j) = -2*(y_f(1) - sqrt(2)*real(alpha)) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
            
            dFs(j) = dFs(j) + M_n_p1(j);
            dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
        %y_2
        elseif j == 3
            dFs(j) = M_2_m2;
            dFs(j) = dFs(j) + K_2_m2 * airy(1,R(j-2),1)/airy(0,R(j-2),1);
            
            dFs(j) = dFs(j) + M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            if j ~= length(y_f)
                dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
                dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
                
                if j ~= length(y_f)-1
                    dFs(j) = dFs(j) + M_n_p1(j);
                    dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
                end
            end
        %y_N-1
        elseif j == length(y_f)-1
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
        %y_N
        elseif j == length(y_f)
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
        %y_n
        else
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
            
            dFs(j) = dFs(j) + M_n_p1(j);
            dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
        end
        
        %deriv fixing
        if isnan(dFs(j))
            dFs(j) = 0;
        end
        if abs(dFs(j)) == inf
            dFs(j) = deriv_limit*sign(dFs(j));
        end        
        if abs(dFs(j)) > deriv_limit
            dFs(j) = deriv_limit*sign(dFs(j));
        end
        if abs(dFs(j)) <= min_limit
            if dFs(j) == 0
                dFs(j) = min_limit;
            else
                dFs(j) = min_limit*sign(dFs(j));
            end
        end
        
        dFs(j) = real(dFs(j)) * Fs;
    end
end

function [ddoty] = derivative_2nd(y_f,delta)
    y_f_forward = [y_f;0;0];
    y_f_back = [0;0;y_f];
    %central difference
    ddoty = (y_f_back - 2*[0;y_f;0] + y_f_forward)/delta^2;
    %trim the tail and head
    ddoty = ddoty(2:end-1);
    %fix head (forward difference), tail not used so we can leave it
    ddoty(1) = (y_f(1) - y_f(2)*2 + y_f(3))/delta^2;
end

function power_1_by_3 = real_1_by_3(operand)
    power_1_by_3 = zeros(size(operand));
    for i = 1:length(operand)
        if sign(operand(i)) == -1
            power_1_by_3(i) = -1*(-operand(i))^(1/3);
        else
            power_1_by_3(i) = operand(i)^(1/3);
        end
    end
end

function vec = get_proposal_vec(length, n)
    if n == 0
        vec = ones(1,length)/sqrt(length);
    else
        vec = zeros(1,length);
        vec(n) = -1/sqrt(2);
        vec(n+1) = 1/sqrt(2);
    end
end
