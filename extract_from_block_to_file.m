%%%%% INFO ABOUT OUR FILE/DATA %%%%%

%define 'j' outside, this will be the folder # we look into: \b#.

%parameters
alpha = 0 + 1i;
lambda = 10;
epsilon = 5;
t_final = 2.5;

%Where to find the data
dir = '\scratch\06943\patrick7\run1_072520';

%output file name
fname15 = 'data_15_new.out';
fname20 = 'data_20_new.out';
fname25 = 'data_25_new.out';
fname30 = 'data_30_new.out';
%Number of monte carlo steps per N
all_num_of_calcs = [5*10^7, 5*10^7, 5*10^7, 5*10^7];
%What are the N's ?
all_N = [15,20,25,30];
%the number of chains per N
run_total = 20;

%%%%% EXTRACTION %%%%%
    %Get the current N
    N_id = floor(j/run_total);
    N = all_N(N_id+1);
    %Get the current run_number
    run_number = j - (run_total * N_id);
    
    %display progress
    if run_number == 0
        disp(['Working on the data for N = ', num2str(N)]);
    end
    
    %file names
    fname1 = [dir, '\b', num2str(j), '\data_', num2str(N), '_', num2str(run_number),  '_y_n.out'];
    fname2 = [dir, '\b', num2str(j), '\data_', num2str(N), '_', num2str(run_number),  '_Fs.out'];
    
    %%%%% INFO ABOUT EXTRACTION %%%%%
    %Holds overall data as we extract for x_n computation
    running_x_n = zeros(1,N+1);
    running_norm = 0;
    %When to begin holding data for x_n computation
    start_pt = 1*10^6;
    %How much data to extract in one go
    chunk_size = 5*10^6;

    %%%%% LOOP OVER FILE CONTENTS %%%%%
    for i = 1:floor(all_num_of_calcs(N_id+1)/chunk_size)+1
        %%%%% EXTRACTING %%%%%
        %if what is left is smaller than each chunk
        if i == floor(all_num_of_calcs(N_id+1)/chunk_size)+1
            extr_pt = mod(all_num_of_calcs(N_id+1),chunk_size);
            data1 = dlmread(fname1,' ', [(i-1)*chunk_size,0, (i-1)*chunk_size + extr_pt,N]);
            data2 = dlmread(fname2,' ', [(i-1)*chunk_size,0, (i-1)*chunk_size + extr_pt,0]);
        %extract a chunk
        else
            data1 = dlmread(fname1,' ', [(i-1)*chunk_size,0, i*chunk_size,N]);
            data2 = dlmread(fname2,' ', [(i-1)*chunk_size,0, i*chunk_size,0]);
        end

        %%%%% HOLDING %%%%%
        %if we don't immediately start holding data
        if start_pt ~= 0
            %if we start holding after this chunk
            if start_pt >= chunk_size
                %update start
                start_pt = start_pt - chunk_size;
            %if there is some data to hold in this chunk
            else
                %hold data
                running_x_n = running_x_n + sum(data1(start_pt:end,:) .* data2(start_pt:end));
                running_norm = running_norm + sum(data2(start_pt:end));
                %update start
                start_pt = 0;
            end
        %hold data
        else
            running_x_n = running_x_n + sum(data1 .* data2);
            running_norm = running_norm + sum(data2);
        end
    end
    %calculate our x_N
    x_N = running_x_n/running_norm;
    %disp(x_N);
    
    %store in appropriate location
    if N == 15
        dlmwrite(fname15, x_N, '-append', 'delimiter', ' ', 'roffset', 0);
    elseif N == 20
        dlmwrite(fname20, x_N, '-append', 'delimiter', ' ', 'roffset', 0);
    elseif N == 25
        dlmwrite(fname25, x_N, '-append', 'delimiter', ' ', 'roffset', 0);
    elseif N == 30
        dlmwrite(fname30, x_N, '-append', 'delimiter', ' ', 'roffset', 0);
    else
        disp('uhoh');
    end