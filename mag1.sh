#!/bin/bash
#SBATCH --job-name=en-serial
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --output=C1-serial.out
#SBATCH --partition=haswell
module load matlab/R2018a
matlab -nodisplay -nosplash < ./single_run_MHMC_reg_keld.m