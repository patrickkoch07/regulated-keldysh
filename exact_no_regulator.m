%H = hbar*w*(n_operator + 1/2) + lambda * x^4
%clear all;

%------------------------- setup -------------------------
%Notes:
%   - |alpha| << cutoff
%   - semi-classical when |alpha| >> 1
savefigure1 = 0;
getx2 = 0;
getx4 = 0;
getp2 = 0;
plotjustx = 0;
plot = 0;
%cutoff = highest n
cutoff = 1000;
%constants
w = 1;
hbar = 1;
mass = 1;
%initial state
%alpha = 0 + 1i;
%amount of weird to the SHO
%lambda = 10;
%time interval
%timesteps = 21;
tf = t_final;
timespace = linspace(0,tf,timesteps);

%------------------------- Initial state -------------------------
%j = transpose(0:cutoff);
%st_alpha = exp(-(abs(alpha)^2)/2)*(alpha.^j)./sqrt(factorial(j));

st_alpha = exp(-(abs(alpha))/2)*ones(cutoff+1,1);
for j = 1:cutoff
    st_alpha(j+1:end) = st_alpha(j+1:end) * alpha/sqrt(j);
end

%initial state normalization
norm = 1/sqrt(ctranspose(st_alpha)*st_alpha);
st_alpha = norm*st_alpha;

%------------------------- Operators -------------------------
X = X_creator(cutoff, w, hbar, mass, alpha);
P = P_creator(cutoff, w, hbar, mass, alpha);
%H = H_creator(cutoff, w, hbar, lambda);
H = H_creator2(cutoff, w, hbar, mass, lambda, alpha);
[evec, eval] = eig(H);

%------------------------- Time Dependence -------------------------
%messages to let everyone know things are still working
disp('beginning calculations');

counter = 0;
x_t = zeros(size(timespace));
p_t = zeros(size(timespace));
%E_t = zeros(size(timespace));

st_alpha_time = st_alpha;
if getx2
    X2 = X*X;
    x2_t = zeros(size(timespace));
end
if getx4
    X4 = X*X*X*X;
    x4_t = zeros(size(timespace));
end
if getp2
    P2 = P*P;
    p2_t = zeros(size(timespace));
end
for t = timespace
    %update message
    counter = counter+1;
    %disp(counter);
    
    %add dependence
    
    %Note the use of "diag(diag( exp_time_factor )".
    %This is because exp(diagonal matrix) will give the values we want for
    %the respective vector elements on the diagonal, but 1.0s everywhere 
    %else. So diag(diag(...)) turns those 1s into 0s and keeps the diagonal
    
    st_alpha_time = evec * (diag(diag(exp(-1i*t*eval/hbar))) * transpose(evec) * st_alpha);
    %used this time evolution because I was scared that the exponential
    %might get too big for bigger alphas
    
    %if counter ~= 1
    %    st_alpha_time = evec * (diag(diag(exp(-1i*(t - timespace(counter-1))*eval/hbar))) * transpose(evec) * st_alpha_time);
    %end
    
    %calculate observables
    x_t(counter) = ctranspose(st_alpha_time) * X * st_alpha_time;
    p_t(counter) = ctranspose(st_alpha_time) * P * st_alpha_time;
    if getx2
        x2_t(counter) = ctranspose(st_alpha_time) * X2 * st_alpha_time;
    end
    if getx4
        x4_t(counter) = ctranspose(st_alpha_time) * X4 * st_alpha_time;
    end
    if getp2
        p2_t(counter) = ctranspose(st_alpha_time) * P2 * st_alpha_time;
    end
end
disp('Done!');

%------------------------- Plot -------------------------
%maximize figure window when ploted
%Note: matlab has some vestigial imaginary terms in the form of:
%x_t = real(x_t) + 10^-30 * 1i
%so the only the real values are graphed.

%get the classical results and plot it
if plotjustx
    figure('units','normalized','outerposition',[0 0 1 1])
    plot(timespace, real(x_t), '-');
    hold on;
    grid on;
elseif ~plot
    %aint gunna plot nothin
else
    [t,y] = classical_result(w, lambda, mass, alpha, tf, hbar);
    plot(t/(1), y(:,1), '-');
    hold on;
    plot(t/(1), y(:,2), '-');
    hold on;
    plot(timespace/(1), real(x_t), 'o');
    hold on;
    plot(timespace/(1), real(p_t), 'o');
    hold on;
    grid on;
    title(['epsilon ',num2str(epsilon),', alpha ', num2str(alpha),', cutoff ', num2str(cutoff), ', timestep ',num2str(timesteps),', hbar ', num2str(hbar),', mass ', num2str(mass), ', w ', num2str(w)]);
    xlabel('time(t/(2*pi))');
    ylabel('expectation values/alpha_0');
    legend('x(t) classical', 'p(t) classical', 'x(t) new', 'p(t) new');
    fig = gcf;
end

if getx2
    figure('units','normalized','outerposition',[0 0 1 1])
    plot(t/(1), y(:,1).^2, '-');
    hold on;
    plot(timespace/(1), x2_t, 'o');
    hold on;
    grid on;
    title(['lambda ',num2str(lambda),', alpha ', num2str(alpha),', cutoff ', num2str(cutoff), ', timestep ',num2str(timesteps),', hbar ', num2str(hbar),', mass ', num2str(mass), ', w ', num2str(w)]);
    xlabel('time(t/(2*pi))');
    ylabel('expectation values');
    legend('x(t)^2 classical', 'x(t)^2 new');
end
if getx4
    figure('units','normalized','outerposition',[0 0 1 1])
    plot(t/(1), y(:,1).^4, '-');
    hold on;
    plot(timespace/(1), x4_t, 'o');
    hold on;
    grid on;
    title(['lambda ',num2str(lambda),', alpha ', num2str(alpha),', cutoff ', num2str(cutoff), ', timestep ',num2str(timesteps),', hbar ', num2str(hbar),', mass ', num2str(mass), ', w ', num2str(w)]);
    xlabel('time(t/(2*pi))');
    ylabel('expectation values');
    legend('x(t)^4 classical', 'x(t)^4 new');
end
if getp2
    figure('units','normalized','outerposition',[0 0 1 1])
    plot(t/(2*pi), y(:,2).^2, '-');
    hold on;
    plot(timespace/(2*pi), p2_t, 'o');
    hold on;
    grid on;
    title(['lambda ',num2str(lambda),', alpha ', num2str(alpha),', cutoff ', num2str(cutoff), ', timestep ',num2str(timesteps),', hbar ', num2str(hbar),', mass ', num2str(mass), ', w ', num2str(w)]);
    xlabel('time(t/(2*pi))');
    ylabel('expectation values');
    legend('p(t)^2 classical', 'p(t)^2 new');
end

if savefigure1
    %save plot
    saveas(fig,['picsofsho\eps', num2str(epsilon), '_alph', num2str(alpha), '_cut', num2str(cutoff), '_ts',num2str(timesteps), '_hbar', num2str(hbar),'_mass', num2str(mass), '_w', num2str(w), '.png']);
    saveas(fig,['picsofsho\eps', num2str(epsilon), '_alph', num2str(alpha), '_cut', num2str(cutoff), '_ts',num2str(timesteps), '_hbar', num2str(hbar),'_mass', num2str(mass), '_w', num2str(w), '.fig']);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%------------------------- Hamiltonian Operator function -------------------------
%remember: H = hbar*w*(n_operator + 1/2) + lambda * x^4
function H = H_creator(cutoff, w, hbar, lambda)
    H_x4 = zeros(cutoff+1,cutoff+1);
    H_sho = zeros(cutoff+1,cutoff+1);
    
    %note: matlab starts at position 1, instead of position 0.
    %Loop over the matrix columns, adding the diagonal and off diagonal terms
    for n = 0:cutoff
        if ((n - 4) >= 0)
            H_x4((n+1)-4,(n+1)) = (1/4)*sqrt(n*(n-1)*(n-2)*(n-3));
        end
        
        if ((n - 2) >= 0)
            %H_x4(n-2,n+1) = (1/4)*((n+1)*sqrt(n*(n-1)) + (n-1)*sqrt(n*(n-1)) + n*sqrt(n*(n-1)) + (n-2)*sqrt(n*(n-1)));
            H_x4((n+1)-2,(n+1)) = (1/4)*sqrt(n*(n-1))*(4*n-2);
        end
        
        H_x4((n+1),(n+1)) = (1/4)*(6*n^2 + 6*n + 3);
        H_sho((n+1),(n+1)) = hbar*w*(n + 1/2);
        
        if ((n + 2) <= cutoff)
            %H_x4((n+1)+2,(n+1)) = (1/4)*((n+1)*sqrt((n+1)*(n+2)) + n*sqrt((n+1)*(n+2)) + (n+3)*sqrt((n+1)*(n+2)) + (n+2)*sqrt((n+1)*(n+2)));
            H_x4((n+1)+2,(n+1)) = (1/4)*sqrt((n+1)*(n+2))*(4*n+6);
        end
        
        if ((n + 4) <= cutoff)
            H_x4((n+1)+4,(n+1)) = (1/4)*sqrt((n+1)*(n+2)*(n+3)*(n+4));
        end
    end
    
    %combine H_x4 and H_sho
    H = H_sho + lambda*H_x4;
end

%------------------------- Hamiltonian Operator function 2 -------------------------
%remember: H = hbar*w*(n_operator + 1/2) + lambda * x^4
%or H = (p'^2/2m + x'^2 *mw/2 + e*x'^4) *alpha^2,where ep = lambda/alpha^2
function H2 = H_creator2(cutoff, w, hbar, m, epsilon, alpha)
    p = P_creator(cutoff, w, hbar, m, alpha);
    x = X_creator(cutoff, w, hbar, m, alpha);
    H2 = p*p/(2*m) + x*x*m*w*w/2 + x*x*x*x*epsilon;
    %H2 = H2*alpha^2;
end

%------------------------- position operator function -------------------------
%remember: X = (a^dag + a)*sqrt(hbar/(2*m*w))
function x = X_creator(cutoff, w, hbar, m, alpha)
    x = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            x((n+1)-1,(n+1)) = sqrt(n);
        end
        if ((n+1) <= cutoff)
            x((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    x = x*sqrt(hbar/(2*m*w));%/alpha;
end

%------------------------- momentum operator function -------------------------
%remember: P = i*(a^dag - a)*sqrt(m*w*hbar/2)
function p = P_creator(cutoff, w, hbar, m, alpha)
    p = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            p((n+1)-1,(n+1)) = -sqrt(n);
        end
        if ((n+1) <= cutoff)
            p((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    p = p*1i*sqrt(m*w*hbar/2);%/alpha;
end

%------------------------- classical result function -------------------------
%remember: H = p^2/2m + mw^2/2*x^2 + lambda*x^4
function [t,y] = classical_result(w, lambda, m, alpha, tf, hbar)
    %initial conditions
    y0 = [real(alpha)*sqrt(2*hbar/(m*w)); imag(alpha)*sqrt(2*hbar/(m*w))];
    
    %system of ODEs
    hamilt_ode_classical = @(t,y) [y(2)/m; -(m*w^2*y(1) + 4*lambda*y(1)^3)];
    
    %solving the ode
    [t,y] = ode45(hamilt_ode_classical, [0, tf], y0);
    %divide by alpha to get x/alpha
    y = y;
end
