%what to compare too (they bring in the value of alpha)
exact_no_regulator;
lindblad;

%initialize
N = 5;
t_final = .1;
dt = t_final/N;
epsilon = 30*dt;

%find initial start
%{
initial = [0;0;0];
false_numOfCalcs = 1*10^5;
for n = 2:N-1
    disp([num2str(n-1), '/', num2str(N-1)]);
    y_n = initial(1:n+1);
    all_y_n = zeros(length(y_n),false_numOfCalcs+1);
    all_Fs = zeros(false_numOfCalcs+1,1);
    
    [dFs, Fs] = distribFunc(y_n, alpha, dt, epsilon);
    all_y_n(:,1) = y_n;
    all_Fs(1) = Fs;
    for j = 1:false_numOfCalcs
        [dFs,Fs,y_n,changes] = update_step(dFs, Fs, y_n, alpha, dt, epsilon, j, changes);
        all_y_n(:,j+1) = y_n;
        all_Fs(j+1) = Fs;
    end
    
    x_n = sum(transpose(all_y_n) .* all_Fs)/sum(all_Fs);
    initial = [transpose(x_n);x_n(end)];
end
%}
number_of_runs = 5;
chicken_calc = zeros(N+1,number_of_runs);
for k = 1:number_of_runs
    %initialize calculation variables
    numOfCalcs = 5*10^5;
    y_n = transpose(real(x_t_lind)+.01);%initial;%transpose(linspace(0,1,N+1));% zeros(N+1,1);
    all_y_n = zeros(length(y_n),numOfCalcs+1);
    all_Fs = zeros(numOfCalcs+1,1);

    x_n_kappa = zeros(size(y_n));
    x_n_er = zeros(size(y_n));
    changes = 0;

    % calculation step 0
    [dFs, Fs] = distrib_function(y_n, alpha, dt, epsilon, lambda);
    all_y_n(:,1) = y_n;
    all_Fs(1) = Fs;

    % calculations
    for j = 1:numOfCalcs
        if j == round((numOfCalcs)/5)
            disp('a fifth is done');
        elseif j == round((numOfCalcs)/5) * 2
            disp('two fifths are done');
        elseif j == round((numOfCalcs)/5) * 3
            disp('three fifths are done');
        elseif j == round((numOfCalcs)/5) * 4
            disp('four fifths are done');
        end

        [dFs,Fs,y_n,changes] = update_step(dFs, Fs, y_n, changes, alpha, dt, epsilon, lambda, j);
        all_y_n(:,j+1) = y_n;
        all_Fs(j+1) = Fs;
    end

    %ignore a bit
    start = 1;
    x_n = sum(transpose (all_y_n(:,start:end)) .* all_Fs(start:end));
    norm = sum(all_Fs(start:end));

    %calculate and display <x(t_n)>, change %, sign %
    disp(norm);
    x_n = x_n/norm;
    disp(x_n);

    disp([num2str(changes*100/round(numOfCalcs)), '% changed']);
    disp(num2str(sum(sign(all_Fs) == 1)/(numOfCalcs+1) * 100));
    
    % {
    figure();
    plot(linspace(0,t_final,N+1),x_n);
    hold on;
    plot(timespace, real(x_t), '-.');
    hold on;
    plot(time, real(x_t_lind), '-.');
    
    figure();
    plot(0:numOfCalcs,log(abs(all_Fs)));
    %}
    
    chicken_calc(:,k) = x_n;
end

%calculate error
%{
for j = 1:length(x_n)
    disp(j);
    [~,~,x_n_er(j),x_n_kappa(j)] = new_Stats(transpose(all_y_n(j,:)) .* sign(all_Fs));
end
%}

%plot
figure('units','normalized','outerposition',[0 0 1 1]);
%errorbar(linspace(0,t_final,N+1), transpose(x_n), x_n_er);
errorbar(linspace(0,t_final,N+1), sum(transpose(chicken_calc)/number_of_runs), std(transpose(chicken_calc))/sqrt(number_of_runs-1))

%dx = dt/100; dy = 0; % displacement so the text does not overlay the data points
%text(linspace(0,t_final,N+1)+dx, x_n+dy, cellstr([num2str(round(x_n_kappa))]), 'Fontsize', 10);

xlabel('time, t');
ylabel('Expectation of x')
title(['Lambda = ', num2str(lambda),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(epsilon/dt)]);
hold on;
plot(timespace, real(x_t), '-.');
hold on;
plot(time, real(x_t_lind), '-.');
legend('monte carlo integration', 'exact values', 'lind values');

%t = annotation('textbox', [.2,.5,.3,.3],'String',{'The Numbers next to the data points are the Kappa values returned from Stats.m'},'FitBoxToText','on');
%t.BackgroundColor = 'w';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dFsNew, FsNew, y_f, changes] = update_step(dFs, Fs, y_f, changes, alpha, delta, epsilon, lambda, index)
    %scales range to fit nicely between min and max std limits
    scalefactor = .4;
    %sets min and max std limits
    min_std = 2 * 10^-2 / 10;
    max_std = 2 * 10^-2 * 10;
    %holds which fourier basis to update
    current = mod(index,length(y_f));
    %holds the basis
    proposal_vec = cos(pi*(0:(length(y_f)-1))/(length(y_f)-1) * current)/sqrt(length(y_f));
    
    %epsilon_samp = 100*delta;
    
    %get range of current position/deriv
    %[dFscurr,Fscurr] = distrib_function(y_f, alpha, delta, epsilon_samp, lambda);
    direc_deriv = sum(dFs .* transpose(proposal_vec));
    range = abs(Fs/direc_deriv) * scalefactor;
    %bound it
    if range > max_std
        range = max_std;
    end
    if range < min_std
        range = min_std;
    end
    
    %use the range to propose new step
    random_jump = real(normrnd(0, range));
    proposal = transpose(proposal_vec) * random_jump;
    %calculate update's range
    [dFsPropose,FsPropose] = distrib_function(y_f + proposal, alpha, delta, epsilon, lambda);
    %[dFsprop,Fsprop] = distrib_function(y_f + proposal, alpha, delta, epsilon_samp, lambda);
    direc_deriv_prop = sum(dFsPropose .* transpose(proposal_vec));
    propose_range = abs(FsPropose/direc_deriv_prop) * scalefactor;
    %bound it, also if we had an update's deriv AND func to be 0, we are at a fringe, so just never accept.
    if isnan(propose_range)
        propose_range = 0;
        y_f = y_f;
        dFsNew = dFs;
        FsNew = Fs;
        return;
    end
    if propose_range > max_std
        propose_range = max_std;
    end
    if propose_range < min_std
        propose_range = min_std;
    end
    
%{
    scalefactor = 10 * 10^-1;
    
    min_std = .5 * (epsilon/delta) * 10^-2 / 1;
    max_std = min_std * 1;
    
    n = length(y_f);
    current = mod(index,n);
    %sum(y_f)
    if current == 0
        %range of values based on current
        range = n * abs(Fs/sum(dFs)) * scalefactor;
        if isnan(range)
            range = 0;
        end
        if range > max_std
            range = max_std;
        end
        if range < min_std
            range = min_std;
        end
        %make the proposal
        proposal = ones(size(y_f));
        proposal = proposal*real(normrnd(0, range)) / sqrt(n);
        
        %find updated range    
        [dFsPropose,FsPropose] = distrib_function(y_f + proposal, alpha, delta, epsilon, lambda);
        propose_range = n * abs(FsPropose/sum(dFsPropose)) * scalefactor;
        if isnan(propose_range)
            propose_range = 0;
        end
        if propose_range > max_std
            propose_range = max_std;
        end
        if propose_range < min_std
            propose_range = min_std;
        end
        
    %y_(n) - y_(n-1)
    else
        %range of values based on current
        range = abs(Fs/(dFs(current) - dFs(current+1))) * scalefactor;
        if isnan(range)
            range = 0;
        end
        if range > max_std
            range = max_std;
        end
        if range < min_std
            range = min_std;
        end
        %make the proposal
        proposal = zeros(size(y_f));
        proposal(current) = -1/sqrt(2);
        proposal(current + 1) = 1/sqrt(2);
        proposal = proposal*real(normrnd(0, range));
        
        %find updated range
            %indicies_of_interest = [mod(index,n),mod(index,n) + 1];
        [dFsPropose,FsPropose] = N_distrib_function(y_f + proposal, alpha, delta, epsilon, lambda, [current,current + 1]);
            %note that whenever the particular index was not updated, it was set to be 0 in dFsPropose. Also 0^0 = 1
        dFsPropose = 0.^(abs(dFsPropose)) .* dFs + dFsPropose;
        propose_range = abs(FsPropose/(dFs(current) - dFs(current+1))) * scalefactor;
        if isnan(propose_range)
            propose_range = inf;
        end
        if propose_range > max_std
            propose_range = max_std;
        end
        if propose_range < min_std
            propose_range = min_std;
        end
        
    end
%}
    
    %calc prob of acceptance based on current position
    prob = abs(FsPropose/Fs);
    %additional conditional prob based on different derivs/update stds
    cond_prob = (normpdf(random_jump, 0, propose_range)/1) / (normpdf(random_jump, 0, range)/1);
    %together, find the prob
    prob = min(1, prob * cond_prob);
    
    %accept or not
    random = rand;
    if random <= prob
        y_f = y_f + proposal;
        dFsNew = dFsPropose;
        FsNew = FsPropose;
        changes = changes + 1;
    else
        dFsNew = dFs;
        FsNew = Fs;
    end
end

function [dFs, Fs] = distrib_function(y_f, alpha, delta, epsilon, lambda)
    limit = 10^50;
    deriv_limit = limit;
    
    min_limit = 10^-10;
        
    %Fixing issues with y_n = 0
    for i = 1:length(y_f)
        if abs(y_f(i)) == 0
            y_f(i) = 10^-4;
        end
        if abs(y_f(i)) < 10^-4
            y_f(i) = 10^-4 * sign(y_f(i));
        end
    end
    %}

    %%%%% Distribution Function's Setup %%%%%
    ddoty = derivative_2nd(y_f,delta);
    %A_n
    A_n = (-delta) * (2*ddoty + 2*y_f + 8*lambda*y_f.^3);
    A_n(1) = (-delta) * (2*ddoty(1) + 2*y_f(1) + 8* lambda *y_f(1).^3) + 2*sqrt(2)*imag(alpha) - 2*(y_f(2)-y_f(1))/delta;
    %B_n
    B_n = ones(size(A_n)) * -epsilon;
    B_n(1) = -1 - epsilon;
    %C_n
    C_n = (-delta)*8*lambda*y_f;
    %G_n
    G_n = A_n + B_n.^2 ./ (3*C_n);
    
    %%%%% Distribution Funcion %%%%%
    %
    Fs = ones(length(y_f)-1, 1);
    
    R = G_n./real_1_by_3(3*C_n);
    if abs(y_f(end)) < .01
        %disp('goofy');
    end
    for j = 1:(length(y_f)-1)
        Fs(j) = 2 * pi * real(airy(0,R(j),1));  
        Fs(j) = Fs(j) * real(exp((-2/3)*(R(j)).^(3/2) - (A_n(j).*B_n(j))./(3*C_n(j)) - (2*B_n(j).^3)./(27*C_n(j).^2)) ./ abs(3*C_n(j)).^(1/3));
    end
    
    Fs = exp(-(y_f(1) - sqrt(2)*real(alpha))^2) * prod(Fs(1:end)); %* exp (-(sqrt(2)*imag(alpha) - (y_f(2) - y_f(1))/delta)^2);
    
    %function fixing
    if isnan(Fs)
        Fs = 0;
    end
    if Fs == inf
        Fs = limit*sign(Fs);
    end
    if Fs == -inf
        Fs = limit*sign(Fs);
    end
    if abs(Fs) > limit
        Fs = limit*sign(Fs);
    end
    
    %%%%% Derivative setup %%%%%
    d_C_n = -delta * 8 * lambda * ones(size(y_f));
    d_A_n = -delta * (2*(-2/delta^2) + 2 + 24*lambda*y_f.^2);
    %because of forward difference
    d_A_n(1) = -delta * (2*(1/delta^2) + 2 + 24*lambda*y_f(1)^2) + 2/delta;
    d_G_n = d_A_n - (B_n.^2 ./ (3*C_n.^2)).*d_C_n;
    
    L_n = ((A_n.*B_n)./(3*C_n.^2) + 4*B_n.^3 ./(27*C_n.^3) - 1./(3*C_n)) .* d_C_n;
    M_n_0 = -B_n./(3*C_n) .* d_A_n;
    K_n_0 = 1./real_1_by_3(3*C_n) .* d_G_n + G_n./(-3*C_n.*real_1_by_3(3*C_n)) .* d_C_n;
    
    d_A_n_m1 = -2*delta*(1/delta^2) * ones(size(A_n));
    %because of forward difference, & A(1) has a normal derivative term
    d_A_n_m1(1) = -2*delta*(-2/delta^2) - 2/delta;
    d_G_n_m1 = d_A_n_m1;
    %recall that M_0_m1 and K_0_m1 aren't used
    M_n_m1 = [0;B_n]./(3 * [0;C_n]) .* [0;d_A_n_m1];
    M_n_m1 = -M_n_m1(1:end-1);
    K_n_m1 = 1 ./ real_1_by_3([0;3*C_n]) .* [0;d_G_n_m1];
    K_n_m1 = K_n_m1(1:end-1);
       
    d_A_n_m1 = -2*delta*(1/delta^2) * ones(size(A_n));
    M_2_m2 = [0;0;B_n]./(3 * [0;0;C_n]) .* [0;0;d_A_n_m1];
    M_2_m2 = -M_2_m2(3);
    K_2_m2 = 1 ./ real_1_by_3([0;0;3*C_n]) .* [0;0;d_A_n_m1];
    K_2_m2 = K_2_m2(3);

    d_A_n_p1 = -2*delta*(1/delta^2) * ones(size(A_n));
    d_G_n_p1 = d_A_n_p1;
    %M_N-1_p1 and K_N-1_p1 aren't used
    M_n_p1 = [B_n;0]./(3 * [C_n;0]) .* [d_A_n_p1;0];
    M_n_p1 = -M_n_p1(2:end);
    K_n_p1 = 1 ./ real_1_by_3([3*C_n;0]) .* [d_G_n_p1;0];
    K_n_p1 = K_n_p1(2:end);
    
    %%%%% Derivative Function %%%%%
    dFs = zeros(size(y_f));
    for j = 1:length(y_f)
        %y_0
        if j == 1
            dFs(j) = -2*(y_f(1) - sqrt(2)*real(alpha)) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
            
            dFs(j) = dFs(j) + M_n_p1(j);
            dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
        %y_2
        elseif j == 3
            dFs(j) = M_2_m2;
            dFs(j) = dFs(j) + K_2_m2 * airy(1,R(j-2),1)/airy(0,R(j-2),1);
            
            dFs(j) = dFs(j) + M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            if j ~= length(y_f)
                dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
                dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
                
                if j ~= length(y_f)-1
                    dFs(j) = dFs(j) + M_n_p1(j);
                    dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
                end
            end
        %y_N-1
        elseif j == length(y_f)-1
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
        %y_N
        elseif j == length(y_f)
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
        %y_n
        else
            dFs(j) = M_n_m1(j);
            dFs(j) = dFs(j) + K_n_m1(j) * airy(1,R(j-1),1)/airy(0,R(j-1),1);
            
            dFs(j) = dFs(j) + L_n(j) + M_n_0(j);
            dFs(j) = dFs(j) + K_n_0(j) * airy(1,R(j),1)/airy(0,R(j),1);
            
            dFs(j) = dFs(j) + M_n_p1(j);
            dFs(j) = dFs(j) + K_n_p1(j) * airy(1,R(j+1),1)/airy(0,R(j+1),1);
        end
        
        %deriv fixing
        if isnan(dFs(j))
            dFs(j) = 0;
        end
        if abs(dFs(j)) == inf
            dFs(j) = deriv_limit*sign(dFs(j));
        end        
        if abs(dFs(j)) > deriv_limit
            dFs(j) = deriv_limit*sign(dFs(j));
        end
        if abs(dFs(j)) <= min_limit
            if dFs(j) == 0
                dFs(j) = min_limit;
            else
                dFs(j) = min_limit*sign(dFs(j));
            end
        end
        
        dFs(j) = real(dFs(j)) * Fs;
    end
end

function [ddoty] = derivative_2nd(y_f,delta)
    y_f_forward = [y_f;0;0];
    y_f_back = [0;0;y_f];
    %central difference
    ddoty = (y_f_back - 2*[0;y_f;0] + y_f_forward)/delta^2;
    %trim the tail and head
    ddoty = ddoty(2:end-1);
    %fix head (forward difference), tail not used so we can leave it
    ddoty(1) = (y_f(1) - y_f(2)*2 + y_f(3))/delta^2;
end

function power_1_by_3 = real_1_by_3(operand)
    power_1_by_3 = zeros(size(operand));
    for i = 1:length(operand)
        if sign(operand(i)) == -1
            power_1_by_3(i) = -1*(-operand(i))^(1/3);
        else
            power_1_by_3(i) = operand(i)^(1/3);
        end
    end
end