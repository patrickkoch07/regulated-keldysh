%what to compare too (they bring in the value of alpha)
exact_no_regulator;
lindblad;

%initialize
epsilon = 10;
N = 20;
t_final = 10;
dt = t_final/N;

%find initial start
%{
initial = [0;0;0];
false_numOfCalcs = 1*10^5;
for n = 2:N-1
    disp([num2str(n-1), '/', num2str(N-1)]);
    y_n = initial(1:n+1);
    all_y_n = zeros(length(y_n),false_numOfCalcs+1);
    all_Fs = zeros(false_numOfCalcs+1,1);
    
    [dFs, Fs] = distribFunc(y_n, alpha, dt, epsilon);
    all_y_n(:,1) = y_n;
    all_Fs(1) = Fs;
    for j = 1:false_numOfCalcs
        [dFs,Fs,y_n,changes] = update_step(dFs, Fs, y_n, alpha, dt, epsilon, j, changes);
        all_y_n(:,j+1) = y_n;
        all_Fs(j+1) = Fs;
    end
    
    x_n = sum(transpose(all_y_n) .* all_Fs)/sum(all_Fs);
    initial = [transpose(x_n);x_n(end)];
end
%}

%initialize calculation variables
numOfCalcs = 5*10^5;
y_n = transpose(real(x_t_lind) + .1);% zeros(N+1,1);
all_y_n = zeros(length(y_n),numOfCalcs+1);
all_Fs = zeros(numOfCalcs+1,1);

x_n_kappa = zeros(size(y_n));
x_n_er = zeros(size(y_n));

changes = 0;

% calculation step 0
[dFs, Fs] = distribFunc(y_n, alpha, dt, epsilon);
all_y_n(:,1) = y_n;
all_Fs(1) = Fs;

% calculations
for j = 1:numOfCalcs
    if j == round((numOfCalcs)/5)
        disp('a fifth is done');
    elseif j == round((numOfCalcs)/5) * 2
        disp('two fifths are done');
    elseif j == round((numOfCalcs)/5) * 3
        disp('three fifths are done');
    elseif j == round((numOfCalcs)/5) * 4
        disp('four fifths are done');
    end
    
    [dFs,Fs,y_n,changes] = update_step(dFs, Fs, y_n, alpha, dt, epsilon, j, changes);
    all_y_n(:,j+1) = y_n;
    all_Fs(j+1) = Fs;
end

%ignore a bit
start = 1;
x_n = sum(transpose (all_y_n(:,start:end)) .* all_Fs(start:end));
norm = sum(all_Fs(start:end));

%calculate and display <x(t_n)>, change %, sign %
x_n = x_n/norm;
disp(x_n);

disp([num2str(changes*100/round(numOfCalcs)), '% changed']);
disp(num2str(sum(sign(all_Fs) == 1)/(numOfCalcs+1) * 100));

%calculate error
for j = 1:length(y_n)
    disp(j);
    [~,~,x_n_er(j),x_n_kappa(j)] = new_Stats(transpose(all_y_n(j,:)) .* sign(all_Fs));
end

%plot
figure('units','normalized','outerposition',[0 0 1 1]);
errorbar(linspace(0,t_final,N+1), x_n, x_n_er);

dx = dt/100; dy = 0; % displacement so the text does not overlay the data points
text(linspace(0,t_final,N+1)+dx, x_n+dy, cellstr(num2str(round(x_n_kappa))), 'Fontsize', 10);

xlabel('time, t');
ylabel('Expectation of x')
title(['Lambda = ', num2str(0),', alpha = ', num2str(alpha), ', N = ', num2str(N), ', Calc Steps = ', num2str(numOfCalcs), ', Epsilon = ', num2str(epsilon)]);
hold on;
plot(timespace, real(x_t), '-.');
hold on;
plot(time, real(x_t_lind), '-.');
legend('monte carlo integration', 'exact values', 'lind values');

t = annotation('textbox', [.2,.5,.3,.3],'String',{'The Numbers next to the data points are the Kappa values returned from Stats.m'},'FitBoxToText','on');
t.BackgroundColor = 'w';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [dFs,Fs,y_n,changes] = update_step(dFs, Fs, y_n, alpha, dt, epsilon, index, changes)
    scalefactor = .6 * 10^-0;
    
    min_std = 12 * (epsilon) * 10^-2 / 1;
    max_std = min_std * 1;
    
    current = mod(index,length(y_n));
    proposal_vec = cos(pi*(0:(length(y_n)-1))/(length(y_n)-1) * current)/sqrt(length(y_n));

    range = abs(Fs/sum(1)) * scalefactor;
    if range > max_std
        range = max_std;
    end
    if range < min_std
        range = min_std;
    end
    
    proposal = transpose(proposal_vec) * real(normrnd(0, range));
    
    [dFsPropose,FsPropose] = distribFunc(y_n+ proposal, alpha, dt, epsilon);
    propose_range = abs(FsPropose/sum(1)) * scalefactor;
    if isnan(propose_range)
        return;
    end
    if propose_range > max_std
        propose_range = max_std;
    end
    if propose_range < min_std
        propose_range = min_std;
    end
%{
    scalefactor = 10 * 10^-1;
    
    min_std = .5 * (epsilon/delta) * 10^-2 / 1;
    max_std = min_std * 1;
    
    n = length(y_f);
    current = mod(index,n);
    %sum(y_f)
    if current == 0
        %range of values based on current
        range = n * abs(Fs/sum(dFs)) * scalefactor;
        if isnan(range)
            range = 0;
        end
        if range > max_std
            range = max_std;
        end
        if range < min_std
            range = min_std;
        end
        %make the proposal
        proposal = ones(size(y_f));
        proposal = proposal*real(normrnd(0, range)) / sqrt(n);
        
        %find updated range    
        [dFsPropose,FsPropose] = distrib_function(y_f + proposal, alpha, delta, epsilon, lambda);
        propose_range = n * abs(FsPropose/sum(dFsPropose)) * scalefactor;
        if isnan(propose_range)
            propose_range = 0;
        end
        if propose_range > max_std
            propose_range = max_std;
        end
        if propose_range < min_std
            propose_range = min_std;
        end
        
    %y_(n) - y_(n-1)
    else
        %range of values based on current
        range = abs(Fs/(dFs(current) - dFs(current+1))) * scalefactor;
        if isnan(range)
            range = 0;
        end
        if range > max_std
            range = max_std;
        end
        if range < min_std
            range = min_std;
        end
        %make the proposal
        proposal = zeros(size(y_f));
        proposal(current) = -1/sqrt(2);
        proposal(current + 1) = 1/sqrt(2);
        proposal = proposal*real(normrnd(0, range));
        
        %find updated range
            %indicies_of_interest = [mod(index,n),mod(index,n) + 1];
        [dFsPropose,FsPropose] = N_distrib_function(y_f + proposal, alpha, delta, epsilon, lambda, [current,current + 1]);
            %note that whenever the particular index was not updated, it was set to be 0 in dFsPropose. Also 0^0 = 1
        dFsPropose = 0.^(abs(dFsPropose)) .* dFs + dFsPropose;
        propose_range = abs(FsPropose/(dFs(current) - dFs(current+1))) * scalefactor;
        if isnan(propose_range)
            propose_range = inf;
        end
        if propose_range > max_std
            propose_range = max_std;
        end
        if propose_range < min_std
            propose_range = min_std;
        end
        
    end
%}
    %calc prob of acceptance
    prob = abs(FsPropose/Fs);
    prob = min(1, prob * normpdf(norm(y_n), norm(y_n)+norm(proposal), propose_range)/normpdf(norm(y_n)+norm(proposal), norm(y_n), range));
    
    %accept or not
    if rand <= prob
        y_n = y_n + proposal;
        dFs = dFsPropose;
        Fs = FsPropose;
        changes = changes + 1;
    end
end

function [dFs,Fs] = distribFunc(y_n, alpha, dt, epsilon)
    Fs = exp(-(y_n(1) - sqrt(2)*real(alpha))^2) * exp (-(sqrt(2)*imag(alpha) - (y_n(2) - y_n(1))/dt)^2);
    
    ddot_y_n = derivative_2nd(y_n,dt);
    
    A_n = -dt * (2*ddot_y_n + 2*y_n);
    A_n(1) = A_n(1) + 2*sqrt(2)*imag(alpha) - 2*(y_n(2) - y_n(1))/dt;
    B_n = -dt*epsilon*ones(size(y_n));
    B_n(1) = -1 + B_n(1);
    
    for j = 2:length(y_n)-1
        Fs = Fs*exp(1/4 * A_n(j).^2 ./ B_n(j));
    end
    
    dFs = 1;
end

function [ddoty] = derivative_2nd(y_f, delta)
    y_f_forward = [y_f;0;0];
    y_f_back = [0;0;y_f];
    %central difference
    ddoty = (y_f_back - 2*[0;y_f;0] + y_f_forward)/delta^2;
    %trim the tail and head
    ddoty = ddoty(2:end-1);
    %fix head (forward difference)
    ddoty(1) = (y_f(1) - y_f(2)*2 + y_f(3))/delta^2;
    ddoty(end) = (y_f(end) - y_f(end-1)*2 + y_f(end-2))/delta^2;
end
