cutoff = 20;

w = 1;
hbar = 1;
mass = 1;
lambda = 10;
epsilon = (10);
alpha_0 = 0 + 1i;

t_final = 1;


    %initial condition
%x_t = zeros(t_steps,0);
st_alpha = exp(-(abs(alpha_0).^2)/2)*ones(cutoff+1,1);
for j = 1:cutoff
    st_alpha(j+1:end) = st_alpha(j+1:end) * alpha_0/sqrt(j);
end
norm = 1/sqrt(ctranspose(st_alpha)*st_alpha);
st_alpha = norm*st_alpha;
%density
rho = st_alpha*ctranspose(st_alpha);
    %end initial condition

    %operators
X = X_creator(cutoff, w, hbar, mass);
%P = P_creator(cutoff, w, hbar, mass);
H = H_creator(cutoff, w, hbar, mass, lambda);
A = a_creator(cutoff);
    %end operators

    %Solver
odefunc = @(t,y) deriv(t,y,H,X,epsilon);
sol = ode45(odefunc,[0.000001 t_final], rho(:));

time = linspace(0.000001,t_final,100);
y = transpose(deval(sol,time));
    %END OF SOLVER

    %calculate <x>
rho_t = reshape(y.',cutoff+1,cutoff+1,[]);
clear x_t_lind
for i = 1:length(time)
    x_t_lind(i) = trace(rho_t(:,:,i)*X);
end
    %End calculate

%figure('units','normalized','outerposition',[0 0 1 1])
hold on;
plot(time,x_t_lind);
title(['epsilon: ',num2str(epsilon),', alpha: ', num2str(alpha_0),', cutoff: ', num2str(cutoff), ', lambda: ', num2str(lambda)]);
xlabel('time(t)');
ylabel('<x(t)>');

function drho_dt = deriv(t,y,H,X,epsilon)
%UNSTACK RHO
    rho = reshape(y, size(H));
%EQUATION
    drho_dt = -1i * (H*rho - rho*H) + (epsilon/1) * (X*rho*ctranspose(X) - .5*((ctranspose(X)*X)*rho + rho*(ctranspose(X)*X)));
%RESTACK
    drho_dt = reshape(drho_dt,size(y)); %drho_dt(:);
end

%------------------------- Hamiltonian Operator function -------------------------
%remember: H = hbar*w*(n_operator + 1/2) + lambda * x^4
function H = H_creator(cutoff, w, hbar, m, lambda)
    p = P_creator(cutoff, w, hbar, m);
    x = X_creator(cutoff, w, hbar, m);
    H = p*p/(2*m) + x*x*m*w*w/2 + x*x*x*x*lambda;
end

%------------------------- position operator function -------------------------
%remember: X = (a^dag + a)*sqrt(hbar/(2*m*w))
function x = X_creator(cutoff, w, hbar, m)
    x = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            x((n+1)-1,(n+1)) = sqrt(n);
        end
        if ((n+1) <= cutoff)
            x((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    x = x*sqrt(hbar/(2*m*w));%/alpha;
end

%------------------------- momentum operator function -------------------------
%remember: P = i*(a^dag - a)*sqrt(m*w*hbar/2)
function p = P_creator(cutoff, w, hbar, m)
    p = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            p((n+1)-1,(n+1)) = -sqrt(n);
        end
        if ((n+1) <= cutoff)
            p((n+1)+1,(n+1)) = sqrt(n+1);
        end
    end
    p = p*1i*sqrt(m*w*hbar/2);%/alpha;
end

function p = a_creator(cutoff)
    p = zeros(cutoff+1,cutoff+1);
    for n = 0:cutoff
        if ((n-1) >= 0)
            p((n+1)-1,(n+1)) = sqrt(n);
        end
    end
end